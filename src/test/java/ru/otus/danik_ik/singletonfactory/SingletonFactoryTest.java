package ru.otus.danik_ik.singletonfactory;

import java.lang.String;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static java.lang.Thread.sleep;
import static java.lang.Thread.yield;
import static org.junit.Assert.*;

public class SingletonFactoryTest
{
    @Before
    public void setup() {
         counter = new AtomicInteger(0);
    }

    @Test
    public void testIt() throws Exception {
        SingletonFactory<Ooo> factory = new SingletonFactory( () -> {
            return new Ooo();
        });

        Collection<Ooo> collection = new ConcurrentLinkedQueue<>();
        List<Thread> threads = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Thread th = new Thread(()->{
                for (int j = 0; j< 10; j++) {
                    collection.add(factory.get());
                    yield();
                }
            });
            th.start();
            threads.add(th);
        }
        for (Thread th: threads) th.join();

        assertEquals(1, counter.get());

        Ooo OneMore = factory.get();
        for (Ooo o: collection)
            assertSame(OneMore, o);
    }

    @Test
    public void testDelayed() throws Exception {
        SingletonFactory<Ooo> factory = new SingletonFactory( () -> {
            return new Ooo();
        });

        Collection<Ooo> collection = new ConcurrentLinkedQueue<>();
        List<Thread> threads = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Thread th = new Thread(()->{
                for (int j = 0; j< 10; j++) {
                    collection.add(factory.get());
                    try {
                        sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            th.start();
            threads.add(th);
        }
        for (Thread th: threads) th.join();

        assertEquals(1, counter.get());

        Ooo OneMore = factory.get();
        for (Ooo o: collection)
            assertSame(OneMore, o);
    }

    private static AtomicInteger counter;

    private class Ooo {
        public Ooo() {
            try {
                counter.incrementAndGet();
                System.out.println("Создание объекта дело такое тяжёлое, долгое...");
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
