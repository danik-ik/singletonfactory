package ru.otus.danik_ik.singletonfactory;

import java.util.function.Supplier;

public class SingletonFactory<T> {
    private final Supplier<T> constructor;
    private volatile Supplier<T> getter = this::createIfNeed;
    private volatile T instance;

    public SingletonFactory(Supplier<T> constructor) {
        this.constructor = constructor;
    }

    public T get() {return getter.get();}

    private synchronized T createIfNeed() {
        if (instance == null) {
            instance = constructor.get();
        }
        getter = this::getExists;
        return instance;
    }

    private T getExists() {
        return instance;
    }
}
